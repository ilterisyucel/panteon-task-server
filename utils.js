const User = require('./User');
module.exports = class Utils {
  constructor(){

  }
  deepEqualCheck(object1, object2){
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length !== keys2.length) {
      return false;
    }
    for (const key of keys1) {
      const val1 = object1[key];
      const val2 = object2[key];
      const areObjects = this.isObject(val1) && isObject(val2);
      if (
        areObjects && !deepEqual(val1, val2) ||
        !areObjects && val1 !== val2
      ) {
        return false;
      }
    }
    return true;
  }
  isObject(object){
    return object != null && typeof object === 'object';
  }

  async documentGenerator(db, collectionName) {
    for(let i = 0; i < 5000; i++){
      const username = this.randomUserName(this.randomInt(4, 14));
      const country = 'Turkey';
      const money = 5000 - i;
      let user = new User(country, username, money);
      const status = await db.createDocument(collectionName, JSON.stringify(user));
      const message = status ? 'Player created succesfully' : 'Cannot create player';
      console.log(message);
    }
  }

  randomUserName(length){
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(this.randomInt(0, charactersLength));
   }
   return result;
  }

  randomInt(min, max){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}