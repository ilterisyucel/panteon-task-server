const Redis = require('ioredis');
const HOST = 'redis-15937.c9.us-east-1-4.ec2.cloud.redislabs.com';
const REDIS_PORT = 15937;
const PASSWORD = '2JjbFbynoichjfFf1JhC7BUNpz9GBOmx'

const SET_KEY = 'plist';

class LeaderBoard {
  constructor(host = HOST, port = REDIS_PORT, password = PASSWORD, setKey = SET_KEY){
    this.setKey = setKey;
    this.isConnected = false;
    this.client = new Redis({
      host: host,
      port: port,
      password: password,
      retryStrategy(times) {
        return 300000;
      },
      // port: port
    });
    this.client.on('connect', () => {
      console.log(`Redis client connect on host: ${this.client.options.host} port: ${this.client.options.port}`);
      this.isConnected = true;
    });
    this.client.on('reconnecting', (event) => {
      console.log(`${event} after reconnect on redis server : ${this.client.options.host}`);
    })
    this.client.on('error', (err) => {
      console.log(`${err} on redis client`);
      console.log(`Could not connect address : ${this.client.options.host}`);
      //this.client.quit();
      this.isConnected = false;
    });
  };
  async addUser(score, obj){
    const status = await this.client.zadd(this.setKey, score, obj);
    return status;
  }
  async setUser(score, user){
    const status = await this.client.set(user, score);
    return status;
  }
  async getAllUser(setKey=this.setKey){
    const result = await this.client.zrange(setKey, 0, -1);
    return result;
  }
  async removeUser(user){

  }
  async getUser(){

  }
}

module.exports.LeaderBoard = LeaderBoard;