const  ws  = require('ws');

module.exports = class Socket {
  constructor(params){
    this.params = params;
    this.socket = new ws.WebSocketServer(this.params);
    this.socket.on('listening', () => {
      console.log('Socket is listen now.');
    });
    this.socket.on('connection', (ws) => {
      console.log('Client Connected.')
      ws.on('close', ()  => console.log('Client Disconnected'));
      ws.on('message', async (_message) => this.messageHandler(_message));
      ws.on('error', (_error) => this.errorHandler(_error));
    });
    this.socket.on('error', (error) => {
      console.log(`${error} on web socket`);
    })
  }
  messageHandler(_message){
    console.log(message);
  }
  errorHandler(_error){
    console.log(_error);
  }
}