const express = require('express');
const cors = require('cors');
const path = require('path');

const PORT = process.env.PORT || 3000;
const corsOptions = {
  origin: '*'
};
const Leaderboard = require('./LeaderBoard.js');
const User = require('./User.js');
const DBConnection = require('./db');
const Utils = require('./utils');
const Socket = require('./socket');

const app = express();
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const lb = new Leaderboard.LeaderBoard();
const db = new DBConnection();
const utils = new Utils();
const socket = new Socket({
  port: 8080
});

const getPlayers = async (rank) => {

  let playerList = [];
  let responseSet = [];
  if(lb.isConnected){
    console.log('Connect Status redis server is OK');
    const redisRecord = await lb.getAllUser('plist');
    if(redisRecord.length > 0){
      redisRecord.forEach((player) => {
        playerList.push(JSON.parse(player));
      });
    }else{
      console.log('Connect Status redis server is OK but records is missing');
      const mongoRecord = await db.getDocuments('plist');
      mongoRecord.forEach((player) => {
        lb.addUser(player.money, JSON.stringify(player));
        playerList.push(player);
      })
      playerList.sort((obj, obj1) => obj1.money - obj.money);
    }
  }else{
    console.log('Could not connect Status redis server');
    const mongoRecord = await db.getDocuments('plist');
    mongoRecord.forEach((player) => {
      playerList.push(player);
    })
    playerList.sort((obj, obj1) => obj1.money - obj.money);    
  }
  if(playerList.length > 100)
    responseSet = new Set(playerList.slice(0, 100));
  else
    responseSet = new Set(playerList.slice());
  try{
    responseSet.add(playerList[rank-3]);
    responseSet.add(playerList[rank-2]);
    responseSet.add(playerList[rank-1]);
    responseSet.add(playerList[rank]);
    responseSet.add(playerList[rank+1]);
    responseSet.add(playerList[rank+2]);
  }catch(error){
    console.error(error);
    return false;
  }

  return responseSet;

}


app.get('/get-players', async (req, res) => {
  const  rank  = parseInt(req.query.rank);
  if(!rank)
    return res.status(400).send('BAD REQUEST!');
  const responseSet = await getPlayers(rank);
  if(!responseSet)
    return res.status(204).send('NO CONTENT');

  return res.status(200).json([...responseSet]);
});

(async() => {
  await db.connect();
  if(db.status === 'OK'){
    app.listen(PORT, async() => {
      console.log(`Server is running on port ${PORT}...`);
      await db.createCollection('plist', {});
      const mongoRecord = await db.getDocuments('plist');
      if(mongoRecord.length <= 0){
        console.log("Mongo DB empty");
        await utils.documentGenerator(db, 'plist');
      }
    });      
  }else{
    console.log(`Server cannot run because db connection fail`);
  }
  
})();

